[_Binary regression_](https://en.wikipedia.org/wiki/Binary_regression)
estimates relationship between one or more explanatory variables
and a single output binary variable. Commonly, the probability
$p(y=1|x)$ of the possible output is modelled.

In a parameteric setting, a common setting is a generalized
linear model, with either [_logit_ (logistic
regression)](https://en.wikipedia.org/wiki/Logistic_regression)
or [_probit_ (probit
regression)](https://en.wikipedia.org/wiki/Probit_model) as the
link function. The theory behind logistic and probit regression
is well developed. The history sections of above Wikipedia pages
give literature links including detailed reviews. 

It is well understood in statistics that in many cases binary
regression should be modelled non-parametrically.
[_Non-parametric logistic regression_ by Trevor Hastie,
1983](https://www.slac.stanford.edu/pubs/slacpubs/3000/slac-pub-3160.pdf)
gives a statistical recipe on binary regression using local
logistic regression on spans over the predictor variable. There
is an overview of in the Discussion section (page 10) with
references to (even) earlier work on non-linear and
non-parametric binary regression. 

A well-studied approach to non-parametric binary regression is
Kernel logistic regression. [_Kernel Logistic Regression and the
Import Vector Machine_ by Ji Shu and Trevor Hastie, 2005](http://dept.stat.lsa.umich.edu/~jizhu/pubs/Zhu-JCGS05.pdf)
provides some results on KLR as well as literature overview. A
different but related approach to non-parametric binary
regression involves Gaussian process prior on the response probability
function. [_Nonparametric binary regression using a Gaussian process prior_ by Nidhan Choudhuri, Subhashis Ghosal, Anindya Roy, 2006](https://www4.stat.ncsu.edu/~sghosal/papers/binary_method.pdf) give a practical description of the method as long as overview of related prior research).

A relatively recent work on non-parametric binary regression 
[_Non-parametric regression for binary dependent variables_ by
Markus Froelich,
2002](http://pages.stern.nyu.edu/~wgreene/DiscreteChoice/Readings/NonparametricBinaryChoice.pdf)
combines a review of prior work and an evaluation on a
real-world dataset (female labor supply). 
